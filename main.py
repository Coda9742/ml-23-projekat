from Classes import *
import pygame
import pygame.freetype
import time
import numpy as np
from collections import defaultdict

AI = True
over10kHands = False
playerCards = []
dealerCards = []
handsWon = 0
handsWonAfter10k = 0
handsLost = 0
handsLostAfter10k = 0

# Dimenzije Ekrana
width = 1920
height = 1080
screen = pygame.display.set_mode((width, height), pygame.RESIZABLE)

# Ime Prozora
pygame.display.set_caption("Blackjack")

# Pozadina
backgroundImage = pygame.image.load('img/background.jpg')
backgroundImage = pygame.transform.scale(backgroundImage, (width, height))

# Zadnji kraj karte
cardBackside = pygame.image.load('Cards/backside.png')
cardBackside = pygame.transform.scale(cardBackside, (150, 300))

pygame.init()

GAME_FONT = pygame.font.Font("Roboto-Light.ttf", 24)

screen.blit(backgroundImage,(0,0))
screen.blit(pygame.image.load('img/HIT.png'), (width-200,20))
screen.blit(pygame.image.load('img/STICK.png'), (width-200,110))
exitButton = pygame.image.load('img/EXIT.png')
exitButton = pygame.transform.scale(exitButton, (150, 150))
screen.blit(exitButton, (width-200,900))

e = 0.1 # Vrednost episola za greedy Monte Carlo algoritam
gamma = 0.95 # Gamma vrednost za Q algoritam
alpha=0.02 # Learning rate za Q algoritam
Q = defaultdict(lambda: np.zeros(2)) # Prazna mapa stanja i akcija
currentHand = [] # Lista odigranih poteza u trenutnoj 'ruci'

def showStats():
	screen.fill(pygame.Color("black"), (80, 800, 160, 40))
	text_surface = GAME_FONT.render("Igracev Zbir: " + str(getPlayerScore()), True, pygame.Color('yellow'))
	screen.blit(text_surface, (80, 800))

	screen.fill(pygame.Color("black"), (80, 500, 200, 150))
	text_surface = GAME_FONT.render("Pobedjenih ruku: " + str(handsWon), True, pygame.Color('yellow'))
	screen.blit(text_surface, (80, 500))
	text_surface = GAME_FONT.render("Izgubljenih ruku: " + str(handsLost), True, pygame.Color('yellow'))
	screen.blit(text_surface, (80, 550))

	try:
		successRate = str(round(handsWon / (handsWon + handsLost) * 100, 2)) + '%'
	except ZeroDivisionError:
		successRate = '/'
	text_surface = GAME_FONT.render("Uspesnost: " + successRate, True, pygame.Color('yellow'))
	screen.blit(text_surface, (80, 600))

def getPlayerScore():
	score = 0
	for card in playerCards:
		score += card.value
	if score > 21:
		for i in range(len(playerCards)):
			if playerCards[i].id[0] == 'A' and playerCards[i].value == 11:
				playerCards[i].value = 1
				score = getPlayerScore()
				break
	return score

def getDealerScore():
	score = 0
	for card in dealerCards:
		score += card.value
	if score > 21:
		for i in range(len(dealerCards)):
			if dealerCards[i].id[0] == 'A' and dealerCards[i].value == 11:
				dealerCards[i].value = 1
				score = getDealerScore()
				break
	return score

def playerHit():
	card = shoe.draw()
	cardImage = card.image
	cardImage = pygame.transform.scale(cardImage, (150, 300))
	screen.blit(cardImage, (500 + len(playerCards) * 150,780))
	playerCards.append(card)

def dealerHit():
	card = shoe.draw()
	cardImage = card.image
	cardImage = pygame.transform.scale(cardImage, (150, 300))
	screen.blit(cardImage, (500 + len(dealerCards) * 150,30))
	dealerCards.append(card)

def dealerDraw(AI = False):
	while getDealerScore() < 17:
		dealerHit()
		pygame.display.update()
		#time.sleep(1)

	dealerScore = getDealerScore()
	playerScore = getPlayerScore()

	if dealerScore > 21:
		playerWin()
		return 1 
	elif dealerScore > playerScore:
		playerBust()
		return -1
	elif dealerScore < playerScore:
		playerWin()
		return 1
	else:
		if AI: return 0
		newHand()

def playerBust(AI = False):
	global handsLost
	global handsLostAfter10k
	handsLost += 1
	if over10kHands: handsLostAfter10k += 1
	showStats()
	pygame.display.update()
	#time.sleep(1)
	if AI: return
	newHand()

def playerWin(AI = False):
	global handsWon
	global handsWonAfter10k
	handsWon += 1
	if over10kHands: handsWonAfter10k += 1
	if AI: return
	newHand()

def newHand():

	
	global playerCards
	global dealerCards
	playerCards = []
	dealerCards = []
	screen.blit(backgroundImage,(0,0))
	screen.blit(pygame.image.load('img/HIT.png'), (width-200,20))
	screen.blit(pygame.image.load('img/STICK.png'), (width-200,110))
	exitButton = pygame.image.load('img/EXIT.png')
	exitButton = pygame.transform.scale(exitButton, (150, 150))
	screen.blit(exitButton, (width-200,900))
	playerHit()
	playerHit()
	dealerHit()
	screen.blit(cardBackside, (650, 30))
	showStats()
	if getPlayerScore() == 21:
		print("BLACKJACK !!!!")
		#time.sleep(1)
		playerWin()

def checkPlayerAce():
	for card in playerCards:
		if card.id[0] == 'A' and card.value == 11:
			return True
	return False

def checkDealerAce():
	for card in dealerCards:
		if card.id[0] == 'A' and card.value == 11:
			return True
	return False

# SVE FUNKCIJE VEZANE ZA AI

# Ova metoda vraca tuple sa 4 elementa u sledecem fomatu
# TUPLE: (IgracevZbir, DilerovZbir, IgracImaKeca, DealerImaKeca)
# IgracImaKeca i DealerImaKeca su boolean parametri koji odredjuju
# da li igrac ili dealer poseduju keca sa vrednoscu 11 koji bi u 
# slucaju prelaska zbira preko 21 postao 1
# Svrha: Zbir 14 sa kartama A3 je znatno drugaciji nego sa kartama J4

def getCurrentState(playerCards, dealerCards):
	return getPlayerScore(), getDealerScore(), checkPlayerAce(), checkDealerAce()

def getAction(state, e, Q):
	hitProb = Q[state][1]
	stayProb = Q[state][0]

	if hitProb > stayProb:
		probs = [e, 1-e]
	elif stayProb > hitProb:
		probs = [1-e, e]
	else:
		probs = [0.5, 0.5]
        
	action = np.random.choice(np.arange(2), p=probs)   
	return action

def setQ(Q, currentEpisode, gamma, alpha):
	reward = currentEpisode[0][2]
	discountRate = gamma * reward
	Lv = discountRate
	Q[currentEpisode[0][0]][currentEpisode[0][1]] += alpha *(Lv - Q[currentEpisode[0][0]][currentEpisode[0][1]])
	return Q

def sortQHardCount(Q):
	keys = list(Q.keys())
	for i in range(len(keys) - 1, -1, -1):
		if keys[i][2] == True:
			del keys[i]
	for i in range(len(keys)):
		min_idx = i
		for j in range(i + 1, len(keys)):
			if keys[j][0] < keys[min_idx][0]:
				min_idx = j
			elif keys[j][0] == keys[min_idx][0]:
				if keys[j][1] < keys[min_idx][1]:
					min_idx = j
		(keys[i], keys[min_idx]) = (keys[min_idx], keys[i])
	return keys

def sortQSoftCount(Q):
	keys = list(Q.keys())
	for i in range(len(keys) - 1, -1, -1):
		if keys[i][2] == False:
			del keys[i]
	for i in range(len(keys)):
		min_idx = i
		for j in range(i + 1, len(keys)):
			if keys[j][0] < keys[min_idx][0]:
				min_idx = j
			elif keys[j][0] == keys[min_idx][0]:
				if keys[j][1] < keys[min_idx][1]:
					min_idx = j
		(keys[i], keys[min_idx]) = (keys[min_idx], keys[i])
	return keys
# Inicijalizacija 'Shoe' (6 spilova)

def printHardTable(keys):
	print('    2 3 4 5 6 7 8 9 10 11')
	keys_idx = 0
	toPrint = ''
	for i in range(4, 21):
		if i < 10:
			toPrint += str(i) + ' | '
		else:
			toPrint += str(i) + '| '
		for j in range(2, 12):
			try:
				if keys[keys_idx][0] == i and keys[keys_idx][1] == j:
					if Q[keys[keys_idx]][0] > Q[keys[keys_idx]][1]:
						toPrint += 'S '
					elif Q[keys[keys_idx]][0] < Q[keys[keys_idx]][1]:
						toPrint += 'H '
					else:
						toPrint += '/ '
					keys_idx += 1
				else:
					toPrint += '/ '
			except IndexError:
				continue
		print(toPrint)
		print()
		toPrint = ''

def printSoftTable(keys):
	print('    2 3 4 5 6 7 8 9 10 11')
	keys_idx = 0
	toPrint = ''
	for i in range(4, 21):
		if i < 10:
			toPrint += str(i) + ' | '
		else:
			toPrint += str(i) + '| '
		for j in range(2, 12):
			try:
				if keys[keys_idx][0] == i and keys[keys_idx][1] == j:
					if Q[keys[keys_idx]][0] > Q[keys[keys_idx]][1]:
						toPrint += 'S '
					elif Q[keys[keys_idx]][0] < Q[keys[keys_idx]][1]:
						toPrint += 'H '
					else:
						toPrint += '/ '
					keys_idx += 1
				else:
					toPrint += '/ '
			except IndexError:
				continue
		print(toPrint)
		print()
		toPrint = ''

shoe = Shoe()

running = True
newHand()
while running:

	if AI:
		currentState = getCurrentState(playerCards, dealerCards)
		action = getAction(currentState, e, Q)
		if action == 1:
			playerHit()
			score = getPlayerScore()
			if score == 21:
				reward = 1
				dealerDraw(True)
			elif score > 21:
				reward = -1
				playerBust(True)
			else:
				reward = 0
				showStats()
				continue
		elif action == 0:
			reward = dealerDraw(True)

		currentHand.append((currentState, action, reward))
		currentHand = np.array(currentHand)
		Q = setQ(Q, currentHand, gamma, alpha)
		currentHand= []
		if handsWon + handsLost >= 10000:
			over10kHands = True
		newHand()


	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			running = False
		if  event.type == pygame.MOUSEBUTTONDOWN:
			mouseX, mouseY = pygame.mouse.get_pos()
			if mouseX>=1720 and mouseX<=1920 and mouseY>=30 and mouseY<=110 and not AI: # HIT DUGME
				playerHit()
				score = getPlayerScore()
				if score == 21:
					dealerDraw()
				elif score > 21:
					playerBust()
				else:
					showStats()
			if mouseX>=1720 and mouseX<=1920 and mouseY>=120 and mouseY<=200 and not AI: # STICK DUGME
				dealerDraw()
			if mouseX>=1725 and mouseX<=1875 and mouseY>=905 and mouseY<=1045: # EXIT DUGME
				running = False
	pygame.display.update()

printHardTable(sortQHardCount(Q))
printSoftTable(sortQSoftCount(Q))

try:
	print('After 10.000 games, success rate is: ' + str(round(handsWonAfter10k / (handsWonAfter10k + handsLostAfter10k) * 100, 2)) + '%')
except ZeroDivisionError:
	print('Number of played hands has not exceeded 10.000')
pygame.quit()
