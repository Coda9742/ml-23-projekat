import random
import pygame


CARDS = ['ACE_OF_DIAMONDS', 'ACE_OF_SPADES', 'ACE_OF_CLUBS', 'ACE_OF_HEARTS',
		'KING_OF_DIAMONDS', 'KING_OF_SPADES', 'KING_OF_CLUBS', 'KING_OF_HEARTS',
		'QUEEN_OF_DIAMONDS', 'QUEEN_OF_SPADES', 'QUEEN_OF_CLUBS', 'QUEEN_OF_HEARTS',
		'JACK_OF_DIAMONDS', 'JACK_OF_SPADES', 'JACK_OF_CLUBS', 'JACK_OF_HEARTS',
		'10_OF_DIAMONDS', '10_OF_SPADES', '10_OF_CLUBS', '10_OF_HEARTS',
		'9_OF_DIAMONDS', '9_OF_SPADES', '9_OF_CLUBS', '9_OF_HEARTS',
		'8_OF_DIAMONDS', '8_OF_SPADES', '8_OF_CLUBS', '8_OF_HEARTS',
		'7_OF_DIAMONDS', '7_OF_SPADES', '7_OF_CLUBS', '7_OF_HEARTS',
		'6_OF_DIAMONDS', '6_OF_SPADES', '6_OF_CLUBS', '6_OF_HEARTS',
		'5_OF_DIAMONDS', '5_OF_SPADES', '5_OF_CLUBS', '5_OF_HEARTS',
		'4_OF_DIAMONDS', '4_OF_SPADES', '4_OF_CLUBS', '4_OF_HEARTS',
		'3_OF_DIAMONDS', '3_OF_SPADES', '3_OF_CLUBS', '3_OF_HEARTS',
		'2_OF_DIAMONDS', '2_OF_SPADES', '2_OF_CLUBS', '2_OF_HEARTS'
		]

def getCardValue(card):

	if card[0] == 'A': return 11
	if card[0] == 'K' or card[0] == 'Q' or card[0] == 'J' or card[0] == '1': return 10
	return int(card[0])

class Shoe():

	def __init__(self):
		self.decks = []
		for i in range(6):
			self.decks.append(Deck())

	def draw(self):
		if self.decks[-1].isEmpty():
			self.decks.pop(len(self.decks) - 1)
		if len(self.decks) <= 1:
			self.refill()
		return self.decks[-1].draw()

	def refill(self):
		self.decks = []
		for i in range(6):
			self.decks.append(Deck())
		print("REFILL")

class Deck():

	def __init__(self):
		self.cards = []
		for cardid in CARDS:
			self.cards.append(Card(cardid))

		self.shuffle()

	def shuffle(self):
		random.shuffle(self.cards)
		random.shuffle(self.cards)
		random.shuffle(self.cards)
		random.shuffle(self.cards)
		random.shuffle(self.cards)
		random.shuffle(self.cards)
		random.shuffle(self.cards)

	def draw(self):
		return self.cards.pop(len(self.cards) -  1)

	def isEmpty(self):
		return not len(self.cards) > 0


class Card():

	def __init__(self, id):
		self.id = id
		self.value = getCardValue(id)
		self.image = pygame.image.load('Cards/'+ self.id.lower() +'.png')

	def __str__(self):
		return self.id + '  ' + str(self.value)

if __name__ == '__main__':
	deck = Deck()
	for card in deck.cards:
		print(card)


	
		